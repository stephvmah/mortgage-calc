# Mortgage Calculator

This project includes a REST API that calculates mortgage payments given a specific set of inputs.

## Running the server

To run the server, run the following commands:

```
    cd server
    npm install
    npm run start:dev
```

## Make an API request

Once the server is running, you can make a POST request to

`localhost:3001/mortgage/payments`

with the body

```
{
    "propertyPrice": 1000000,
    "downPayment": 300000,
    "annualInterestRate": 0.04,
    "amortizationPeriod": 25,
    "paymentSchedule": "monthly"
}
```

This request will return the monthly mortgage payment to the user.

## Running the Client

To run the client, run the following commands:

```
    cd client
    npm install
    npm run start
```

The client is a React-based web app that will make requests to the Mortgage Calculator REST API and show the results on screen. Both the client and server must be running for this to work.


import './App.css';
import { MortgageCalculator } from './components';

function App() {
  return (
    <div className="App">
      <MortgageCalculator />
    </div>
  );
}

export default App;

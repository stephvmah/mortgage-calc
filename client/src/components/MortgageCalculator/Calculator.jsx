import React, { useState } from 'react';
import { Paper } from '@mui/material';
import { MortgageCalculatorForm } from './Form';
import { MortgagePayments } from './Payments';
import styled from '@emotion/styled';

const StyledContainer = styled(Paper)`
    width: 80%;
    margin: 1rem auto;
`;
export const MortgageCalculator = () => {
    const [payments, setPayments] = useState();

    return (
        <StyledContainer>
            <MortgageCalculatorForm setPayments={setPayments} />
            {payments && <MortgagePayments payments={payments} />}
        </StyledContainer>
    );
};
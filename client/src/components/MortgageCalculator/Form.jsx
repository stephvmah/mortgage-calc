import React, { useState } from 'react';
import { Box, Button, TextField, MenuItem, InputAdornment, Alert } from '@mui/material';
import styled from '@emotion/styled';

const StyledContainer = styled(Box)`
    display: flex;
    flex-direction: column;
    padding: 1rem;
    column-gap: 1rem;
    row-gap: 1rem;
`;

const StyledButtonContainer = styled(Box)`
    display: flex;
    column-gap: 1rem;
    justify-content: center;
`

export const MortgageCalculatorForm = ({ setPayments }) => {
    const [propertyPrice, setPropertyPrice] = useState(0);
    const [downPayment, setDownPayment] = useState(0);
    const [annualInterestRate, setAnnualInterestRate] = useState(0);
    const [amortizationPeriod, setAmortizationPeriod] = useState(5);
    const [paymentSchedule, setPaymentSchedule] = useState('monthly');
    const [errors, setErrors] = useState();

    const handleSubmit = async (e) => {
        e.preventDefault();

        const inputs = {
            propertyPrice,
            downPayment,
            annualInterestRate: annualInterestRate / 100,
            amortizationPeriod,
            paymentSchedule
        };

        const response = await fetch('http://localhost:3001/mortgage/payments',
            {
                method: 'POST',
                headers: {
                    'content-type': 'application/json'
                },
                body: JSON.stringify(inputs)
            });

        const data = await response.json();
        if (response.status === 200) {
            setErrors();
            setPayments(data.payments);
        } else {
            setErrors(data.message);
            setPayments();
        }

    };

    const handleClear = () => {
        setPropertyPrice(0);
        setDownPayment(0);
        setAnnualInterestRate(0);
        setAmortizationPeriod(5);
        setPaymentSchedule('monthly');
        setPayments();
        setErrors();
    }

    return (
        <StyledContainer
            component='form'
            onSubmit={handleSubmit}
        >
            <TextField
                id='propertyPrice'
                label='Property Price'
                value={propertyPrice}
                onChange={(e) => setPropertyPrice(e.target.value)}
                InputProps={{
                    startAdornment: <InputAdornment position="start">$</InputAdornment>,
                }}
            />
            <TextField
                id='downPayment'
                label='Down Payment'
                value={downPayment}
                onChange={(e) => setDownPayment(e.target.value)}
                InputProps={{
                    startAdornment: <InputAdornment position="start">$</InputAdornment>,
                }}
            />
            <TextField
                id='annualInterestRate'
                label='Annual Interest Rate'
                value={annualInterestRate}
                onChange={(e) => setAnnualInterestRate(e.target.value)}
                InputProps={{
                    endAdornment: <InputAdornment position="end">%</InputAdornment>,
                }}
            />
            <TextField
                id='amortizationPeriod'
                label='Amortization Period'
                value={amortizationPeriod}
                onChange={(e) => setAmortizationPeriod(e.target.value)}
                InputProps={{
                    endAdornment: <InputAdornment position="end">years</InputAdornment>,
                }}
            />
            <TextField
                id='paymentSchedule'
                label='PaymentSchedule'
                value={paymentSchedule}
                onChange={(e) => setPaymentSchedule(e.target.value)}
                select
            >
                <MenuItem value='accelerated biweekly'>Accelerated Bi-weekly</MenuItem>
                <MenuItem value='biweekly'>Bi-weekly</MenuItem>
                <MenuItem value='monthly'>Monthly</MenuItem>
            </TextField>
            <StyledButtonContainer>
                <Button variant='contained' onClick={handleClear}>Clear</Button>
                <Button type='submit' variant='contained'>Submit</Button>
            </StyledButtonContainer>
            {errors && <Alert severity='error'>{errors}</Alert>}
        </StyledContainer>
    );
};
import React from 'react';
import { Box } from '@mui/material';
import styled from '@emotion/styled';

const StyledContainer = styled(Box)`
    display: flex;
    flex-direction: column;
    padding: 1rem;
    row-gap: 1rem;
`;


export const MortgagePayments = ({ payments }) => {
    return (
        <StyledContainer>
            Your payments will be <strong>${payments}</strong>
        </StyledContainer>
    );
};
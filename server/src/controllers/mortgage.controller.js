import { Router } from 'express';
import { MortgageService } from '../services/mortgage.service';

const router = Router();
const mortgageService = new MortgageService();

router.post('/payments', async (req, res, next) => {
    try {
        const payments = await mortgageService.getPaymentsPerPaymentSchedule(req.body);
        res.send({ payments })
    } catch (e) {
        next(e)
    }
});

export default router;
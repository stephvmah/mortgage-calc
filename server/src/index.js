import express, { json } from 'express';
import cors from 'cors';
import mortgageController from './controllers/mortgage.controller';
import errorHandler from './helpers/errorHandler';

const app = express();
const PORT = process.env.PORT || 3001;

app.use(cors());
app.use(json());
app.use('/mortgage', mortgageController);
app.use(errorHandler);

app.listen(PORT, () => {
  console.log(`REST API listening on port ${PORT}`);
});
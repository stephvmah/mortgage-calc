import Joi from 'joi';

export const mortgagePaymentInputSchema = Joi.object({
    propertyPrice: Joi.number()
        .positive()
        .required(),

    downPayment: Joi.number()
        .positive()
        .less(Joi.ref('propertyPrice'))
        .required(),

    annualInterestRate: Joi.number()
        .less(1)
        .positive()
        .required(),

    amortizationPeriod: Joi.number()
        .integer()
        .positive()
        .min(5)
        .max(30)
        .multiple(5)
        .required(),

    paymentSchedule: Joi.string()
        .valid('accelerated biweekly', 'biweekly', 'monthly')
        .required()
})
import { mortgagePaymentInputSchema } from '../schemas/mortgage'
import { BadRequest } from 'http-errors';

export class MortgageService {
    async getPaymentsPerPaymentSchedule(inputs) {
        await this.validateMortgagePaymentInputs(inputs);

        return this.calculatePayments(inputs);
    }

    async validateMortgagePaymentInputs(inputs) {
        try {
            await mortgagePaymentInputSchema.validateAsync(inputs, { abortEarly: false });
        } catch (e) {
            throw new BadRequest(`Mortgage Payment inputs are malformed: ${e.message}`);
        }
    }

    calculatePayments({ propertyPrice, downPayment, annualInterestRate, amortizationPeriod, paymentSchedule }) {
        const numAnnualPayments = this.getNumAnnualPayments(paymentSchedule);
        const n = numAnnualPayments * amortizationPeriod; // total number of pay periods
        const P = propertyPrice - downPayment; // principal
        const r = annualInterestRate / numAnnualPayments; // per payment schedule interest rate

        const compoundInterestRate = Math.pow(1 + r, n)

        return (P * ((r * compoundInterestRate) / (compoundInterestRate - 1))).toFixed(2)
    }

    getNumAnnualPayments(paymentSchedule) {
        switch (paymentSchedule) {
            case 'accelerated biweekly':
                return 26;
            case 'biweekly': // ASSUMPTION: bi-weekly refers to "semi-monthly", not actually biweekly
                return 24;
            case 'monthly':
            default:
                return 12;
        }
    }
}
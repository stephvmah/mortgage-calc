import { MortgageService } from '../services/mortgage.service';

describe('Mortgage Service', () => {
    const mortgageService = new MortgageService();
    describe('getPaymentsPerPaymentSchedule', () => {
        it('throws an error if any fields are missing', async () => {
            await expect(mortgageService.getPaymentsPerPaymentSchedule({}))
                .rejects.toThrow('Mortgage Payment inputs are malformed: \"propertyPrice\" is required. \"downPayment\" is required. \"annualInterestRate\" is required. \"amortizationPeriod\" is required. \"paymentSchedule\" is required');
        });

        it('throws an error if numerical fields are not numbers', async () => {
            const inputs = {
                propertyPrice: 'hi',
                downPayment: 'hey',
                annualInterestRate: 'hello',
                amortizationPeriod: 'yo',
                paymentSchedule: 'monthly'
            };
            await expect(mortgageService.getPaymentsPerPaymentSchedule(inputs))
                .rejects.toThrow('Mortgage Payment inputs are malformed: \"propertyPrice\" must be a number. \"downPayment\" must be a number. \"annualInterestRate\" must be a number. \"amortizationPeriod\" must be a number');
        });

        it('throws an error if numerical fields are not positive', async () => {
            const inputs = {
                propertyPrice: -5,
                downPayment: -10,
                annualInterestRate: -0.5,
                amortizationPeriod: -15,
                paymentSchedule: 'monthly'
            };
            await expect(mortgageService.getPaymentsPerPaymentSchedule(inputs))
                .rejects.toThrow('Mortgage Payment inputs are malformed: \"propertyPrice\" must be a positive number. \"downPayment\" must be a positive number. \"annualInterestRate\" must be a positive number. \"amortizationPeriod\" must be a positive number. \"amortizationPeriod\" must be greater than or equal to 5');
        });

        it('throws an error if downPayment is more than property price', async () => {
            const inputs = {
                propertyPrice: 10,
                downPayment: 11,
                annualInterestRate: 0.01,
                amortizationPeriod: 15,
                paymentSchedule: 'monthly'
            };
            await expect(mortgageService.getPaymentsPerPaymentSchedule(inputs))
                .rejects.toThrow('Mortgage Payment inputs are malformed: \"downPayment\" must be less than ref:propertyPrice');
        });

        it('throws an error if annualInterestRate is equal to or greater than 1', async () => {
            const inputs = {
                propertyPrice: 10,
                downPayment: 2,
                annualInterestRate: 1.1,
                amortizationPeriod: 15,
                paymentSchedule: 'monthly'
            };
            await expect(mortgageService.getPaymentsPerPaymentSchedule(inputs))
                .rejects.toThrow('Mortgage Payment inputs are malformed: \"annualInterestRate\" must be less than 1');
        });

        it('throws an error if amortizationPeriod is not an integer', async () => {
            const inputs = {
                propertyPrice: 10,
                downPayment: 2,
                annualInterestRate: 0.01,
                amortizationPeriod: 5.5,
                paymentSchedule: 'monthly'
            };
            await expect(mortgageService.getPaymentsPerPaymentSchedule(inputs))
                .rejects.toThrow('Mortgage Payment inputs are malformed: \"amortizationPeriod\" must be an integer. \"amortizationPeriod\" must be a multiple of 5');
        });

        it('throws an error if amortizationPeriod is less than 5', async () => {
            const inputs = {
                propertyPrice: 10,
                downPayment: 2,
                annualInterestRate: 0.01,
                amortizationPeriod: 1,
                paymentSchedule: 'monthly'
            };
            await expect(mortgageService.getPaymentsPerPaymentSchedule(inputs))
                .rejects.toThrow('Mortgage Payment inputs are malformed: \"amortizationPeriod\" must be greater than or equal to 5. \"amortizationPeriod\" must be a multiple of 5');
        });

        it('throws an error if amortizationPeriod is greater than 30', async () => {
            const inputs = {
                propertyPrice: 10,
                downPayment: 2,
                annualInterestRate: 0.01,
                amortizationPeriod: 35,
                paymentSchedule: 'monthly'
            };
            await expect(mortgageService.getPaymentsPerPaymentSchedule(inputs))
                .rejects.toThrow('Mortgage Payment inputs are malformed: \"amortizationPeriod\" must be less than or equal to 30');
        });

        it('throws an error if amortizationPeriod is not a multiple of 5', async () => {
            const inputs = {
                propertyPrice: 10,
                downPayment: 2,
                annualInterestRate: 0.01,
                amortizationPeriod: 7,
                paymentSchedule: 'monthly'
            };
            await expect(mortgageService.getPaymentsPerPaymentSchedule(inputs))
                .rejects.toThrow('Mortgage Payment inputs are malformed: \"amortizationPeriod\" must be a multiple of 5');
        });

        it('throws an error if paymentSchedule is not a string', async () => {
            const inputs = {
                propertyPrice: 10,
                downPayment: 2,
                annualInterestRate: 0.01,
                amortizationPeriod: 25,
                paymentSchedule: 1
            };
            await expect(mortgageService.getPaymentsPerPaymentSchedule(inputs))
                .rejects.toThrow('Mortgage Payment inputs are malformed: \"paymentSchedule\" must be one of [accelerated biweekly, biweekly, monthly]. \"paymentSchedule\" must be a string');
        });

        it('throws an error if paymentSchedule is not in the list of allowed strings', async () => {
            const inputs = {
                propertyPrice: 10,
                downPayment: 2,
                annualInterestRate: 0.01,
                amortizationPeriod: 25,
                paymentSchedule: 'yearly'
            };
            await expect(mortgageService.getPaymentsPerPaymentSchedule(inputs))
                .rejects.toThrow('Mortgage Payment inputs are malformed: \"paymentSchedule\" must be one of [accelerated biweekly, biweekly, monthly]');
        });

        it('returns a valid payment for an accelerated biweekly schedule', async () => {
            const inputs = {
                propertyPrice: 100000,
                downPayment: 30000,
                annualInterestRate: 0.01,
                amortizationPeriod: 25,
                paymentSchedule: 'accelerated biweekly'
            };
            const expectedPayment = await mortgageService.getPaymentsPerPaymentSchedule(inputs);

            expect(Number(expectedPayment)).toBeCloseTo(121.73);
        });

        it('returns a valid payment for an biweekly schedule', async () => {
            const inputs = {
                propertyPrice: 100000,
                downPayment: 30000,
                annualInterestRate: 0.01,
                amortizationPeriod: 25,
                paymentSchedule: 'biweekly'
            };
            const expectedPayment = await mortgageService.getPaymentsPerPaymentSchedule(inputs);

            expect(Number(expectedPayment)).toBeCloseTo(131.88);
        });

        it('returns a valid payment for an monthly schedule', async () => {
            const inputs = {
                propertyPrice: 100000,
                downPayment: 30000,
                annualInterestRate: 0.01,
                amortizationPeriod: 25,
                paymentSchedule: 'monthly'
            };
            const expectedPayment = await mortgageService.getPaymentsPerPaymentSchedule(inputs);

            expect(Number(expectedPayment)).toBeCloseTo(263.81);
        });
    });
})